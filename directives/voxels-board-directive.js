/**
 * Created by ebates on 7/10/2015.
 */
angular.module("voxels").directive("voxelBox", ["VoxelsData", function(VoxelsData){
    return{
        restrict: "E",
        template: '<canvas width="512" height="512"></canvas><span>{{coordinates}}</span>',
        link: function(scope, element, attrs){
            scope.coordinates = "blah";
            var ctx = element[0].children[0].getContext("2d");
            var scaleX = 40;
            var scaleY = 32;
            var halfScaleX = scaleX / 2;
            var halfScaleY = scaleY / 2;
            //var halfScale = scale / 2;
            var offsetX = 200; // this is just for centering prettiness


            var allLayers = VoxelsData.ListLayers();

            var forEachBlock = function(toDo){
                for(var z = 0; z < allLayers.length; z++){
                    var thisLayer = allLayers[z];
                    for(var x = 0;  x < thisLayer.length; x++){
                        var thisRow = thisLayer[x];
                        for(var y = 0; y < thisRow.length; y++){
                            var thisBlock = allLayers[z][x][y];
                            if(thisBlock){
                                toDo(thisBlock, x, y, z);
                            }
                        }
                    }
                }

            };

            var getScreenCoords = function(mapx, mapy, mapz){
                var screen = {};

                screen.x = (mapx - mapy) * halfScaleX;
                screen.y = (mapx + mapy - mapz) * halfScaleY;

                return screen;
            };

            var getScreenCoordsFromCubePart = function(cubepart, translateX, translateY){
                var screen = {};

                screen.x = (cubepart.x - cubepart.y) * halfScaleX + translateX;
                screen.y = (cubepart.x + cubepart.y + cubepart.z) * halfScaleY + translateY;

                return screen;
            };

            var getMapCoordsFromScreenCoords = function(screenx, screeny){
                var map = {};

                map.x = (screenx / halfScaleX + screeny / halfScaleY) /2;
                map.y = (screeny / halfScaleY - (screenx / halfScaleX)) /2;

                return map;
            };


            var refreshScreen = function(){
                ctx.globalAlpha = 1;

                ctx.fillStyle = "#FFFFFF";
                ctx.fillRect(0, 0, 512, 512);

                var cubeParts = {
                    bottomLeft: {
                        x: 0,
                        y: 1,
                        z: 1
                    },
                    bottomMiddle: {
                        x: 1,
                        y: 1,
                        z: 1
                    },
                    bottomRight: {
                        x: 1,
                        y: 0,
                        z: 1
                    },
                    topLeft: {
                        x: 0,
                        y: 1,
                        z: 0
                    },
                    topRight: {
                        x: 1,
                        y: 0,
                        z: 0
                    },
                    topMiddle: {
                        x: 1,
                        y: 1,
                        z: 0
                    },
                    topBack:{
                        x: 0,
                        y: 0,
                        z: 0
                    }
                };

                forEachBlock(function(block, x, y, z){

                    var coords = getScreenCoords(x, y, z);
                    var originX = coords.x + offsetX;
                    var originY = coords.y;

                    ctx.strokeStyle = "#000000";
                    ctx.globalAlpha = block.front.opacity;
                    var topRight = getScreenCoordsFromCubePart(cubeParts.topMiddle, originX, originY);
                    var bottomRight = getScreenCoordsFromCubePart(cubeParts.bottomMiddle, originX, originY);
                    var bottomLeft = getScreenCoordsFromCubePart(cubeParts.bottomLeft, originX, originY);
                    var topLeft = getScreenCoordsFromCubePart(cubeParts.topLeft, originX, originY);
                    ctx.beginPath();
                    ctx.moveTo(topRight.x, topRight.y);
                    ctx.lineTo(bottomRight.x, bottomRight.y);
                    ctx.lineTo(bottomLeft.x, bottomLeft.y);
                    ctx.lineTo(topLeft.x, topLeft.y);
                    ctx.lineTo(topRight.x, topRight.y);
                    ctx.closePath();
                    ctx.fillStyle = block.front.color;
                    ctx.fill();
                    //ctx.stroke();

                    ctx.globalAlpha = block.right.opacity;
                    topRight = getScreenCoordsFromCubePart(cubeParts.topRight, originX, originY);
                    bottomRight = getScreenCoordsFromCubePart(cubeParts.bottomRight, originX, originY);
                    bottomLeft = getScreenCoordsFromCubePart(cubeParts.bottomMiddle, originX, originY);
                    topLeft = getScreenCoordsFromCubePart(cubeParts.topMiddle, originX, originY);
                    ctx.beginPath();
                    ctx.moveTo(topRight.x, topRight.y);
                    ctx.lineTo(bottomRight.x, bottomRight.y);
                    ctx.lineTo(bottomLeft.x, bottomLeft.y);
                    ctx.lineTo(topLeft.x, topLeft.y);
                    ctx.lineTo(topRight.x, topRight.y);
                    ctx.closePath();
                    ctx.fillStyle = block.right.color;
                    ctx.fill();
                    //ctx.stroke();

                    ctx.globalAlpha = block.top.opacity;
                    topRight = getScreenCoordsFromCubePart(cubeParts.topBack, originX, originY);
                    bottomRight = getScreenCoordsFromCubePart(cubeParts.topRight, originX, originY);
                    bottomLeft = getScreenCoordsFromCubePart(cubeParts.topMiddle, originX, originY);
                    topLeft = getScreenCoordsFromCubePart(cubeParts.topLeft, originX, originY);
                    ctx.beginPath();
                    ctx.moveTo(topRight.x, topRight.y);
                    ctx.lineTo(bottomRight.x, bottomRight.y);
                    ctx.lineTo(bottomLeft.x, bottomLeft.y);
                    ctx.lineTo(topLeft.x, topLeft.y);
                    ctx.lineTo(topRight.x, topRight.y);
                    ctx.closePath();
                    ctx.fillStyle = block.top.color;
                    ctx.fill();
                    //ctx.stroke();
                });

            };

            refreshScreen();

            element.on('mousemove', function(event){
                var tileCoords = getMapCoordsFromScreenCoords(event.clientX -offsetX, event.clientY);
                scope.coordinates = ("x:" + event.x + ", y:" + event.y);
                scope.$apply();

                for(var z = 0; z < 4; z++){
                    var thisLayer = allLayers[z];

                    for(var i = 0; i < thisLayer.length; i++){
                        for(var j = 0; j <thisLayer[i].length; j++){
                            var clearBlock = thisLayer[i][j];
                            if(clearBlock){
                                //clearBlock.top.originalColor = clearBlock.top.originalColor || clearblock.top.color + "";
                                clearBlock.top.color = clearBlock.top.originalColor || clearBlock.top.color;
                            }
                        }
                    }

                    var thisRow = thisLayer[Math.floor(tileCoords.x)];
                    if(thisRow){
                        var thisBlock = thisRow[Math.floor(tileCoords.y)];
                        if(thisBlock) {
                            thisBlock.top.originalColor = thisBlock.top.originalColor || thisBlock.top.color;
                            thisBlock.top.color = "#2222FF";
                            refreshScreen();
                        }
                    }


                }
            });

            element.on('click', function(event){

                var tileCoords = getMapCoordsFromScreenCoords(event.clientX, event.clientY);
                for(var z = 0; z < 4; z++){
                    var thisLayer = allLayers[z];
                    var thisRow = thisLayer[Math.floor(tileCoords.x)];
                    if(thisRow){
                        var thisBlock = thisRow[Math.floor(tileCoords.y)];
                        if(thisBlock) {
                            thisBlock.top.color = '#0000FF'
                            refreshScreen();
                        }
                    }


                }
            });
        }
    };
}]);