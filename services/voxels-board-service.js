/**
 * Created by ebates on 7/10/2015.
 */
angular.module("voxels").factory("VoxelsData", [function(){

    var layers=[];

    var fakeLayers = [
        [
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3],
            [3, 3, 3, 3, 3, 3, 3, 3]
        ],
        [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 1, 1, 0, 0],
            [0, 1, 0, 1, 1, 0, 1, 0],
            [0, 0, 0, 1, 1, 0, 0, 0],
            [0, 1, 0, 1, 1, 0, 1, 0],
            [0, 0, 1, 1, 1, 1, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 0, 0, 0, 0],
            [0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 1, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 2, 1, 1, 2, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0]
        ]
    ];


    for(var z = 0; z < 8; z++){
        var newLayer = [];
        for(var x = 0; x < 8; x++){
            var newRow = [];
            for(var y = 0; y < 8; y++){
               newRow.push(null);
            }
            newLayer.push(newRow);
        }
        layers.push(newLayer);
    }


    for(var z = fakeLayers.length - 1; z >= 0; z--){
        var thisLayer = fakeLayers[z];
        for(var x = 0; x < thisLayer.length; x++){
            var thisRow = thisLayer[x];
            for(var y = 0; y < thisRow.length; y++){
                var thisBlock = thisRow[y];
                if(thisBlock > 0) {

                    var newBlock = {
                        top: {
                            color: "#FF2222",
                            opacity:1
                        },
                        right: {
                            color: '#CC0000',
                            opacity: 1
                        },
                        front: {
                            color: '#660000',
                            opacity: 1
                        }
                    };

                    if(thisBlock == 2){
                        newBlock.front.opacity = 1;
                        newBlock.front.color = "#00FF00";
                    }

                    if(thisBlock == 3){
                        newBlock.top.opacity = .5;
                        newBlock.front.opacity = 0;
                        newBlock.right.opacity = 0;
                        newBlock.top.color="#000000"
                    }

                    layers[z][x][y] = newBlock;
                }

            }
        }
    }

    return{
        ListLayers: function(){
            return layers;
        }
    }
}]);

/*


newRow.push({
    top: {
        color: "#FF0000",
        opacity: 1
    },
    right: {
        color: '#CC0000',
        opacity: 1
    },
    front: {
        color: '#660000',
        opacity: 1
    }
});*/
